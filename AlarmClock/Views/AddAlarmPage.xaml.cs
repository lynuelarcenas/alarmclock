﻿using System;
using AlarmClock.Utilities.Helpers.DatabaseReader;
using Plugin.LocalNotifications;
using Xamarin.Forms;

namespace AlarmClock.Views
{
    public partial class AddAlarmPage : ContentPage
    {
        
        DBReader dBReader = DBReader.GetInstance;
        Models.Time alarm;
        int timeId = 0;
        //string alarmTime;
        //int hour;
        //string minutes;

        public AddAlarmPage()
        {
            InitializeComponent();
            BindingContext = alarm;
            this.alarm = alarm;
        }

        //void Time_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    var hour = (timePicker.Time.Hours > 12) ? (timePicker.Time.Hours - 12) : (timePicker.Time.Hours);
        //    var minutes = (timePicker.Time.Minutes > 10) ? timePicker.Time.Minutes.ToString() : "0" + timePicker.Time.Minutes;
        //    var alarmTime = hour + " : " + minutes;
        //    Console.WriteLine(alarmTime);
        //}




        async void Save_Clicked(object sender, System.EventArgs e)
        {
            alarm = new Models.Time() { Id = timeId, /*Hour = hour, Minutes = minutes, AlarmTime = alarmTime, */IsActive = true, TimePicked = timePicker.Time };
            //Console.WriteLine("timeID: " + timeID);
            await dBReader.SaveItemAsync<Models.Time>(alarm, "Time", timeId);
            //await databasReader.SaveItemAsync<Models.Alarm>(alarm, "Alarm", autoIncId);

            //CrossLocalNotifications.Current.Show("Alarm", "Wake Up", timeId, DateTime.Today + timePicker.Time);
            await Navigation.PopAsync();
            timeId++;
            //Console.WriteLine(timePicker.Time.Hours);
            //Console.WriteLine(timePicker.Time.Minutes);
            Console.WriteLine(alarm.Id);
            Console.WriteLine(alarm.IsActive);
        }


    }
}
