﻿using System;
using SQLite;

namespace AlarmClock.Models
{
    public class Time
    {
        

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        //public int Hour { get; set; }
        //public string Minutes { get; set; }
        //public string AlarmTime { get; set; }
        public bool IsActive { get; set; }
        public TimeSpan TimePicked { get; set; }
    }
}
