﻿using System;
using System.Collections.Generic;
using System.Threading;
using AlarmClock.Models;
using AlarmClock.Utilities.Helpers.DatabaseReader;
using Newtonsoft.Json.Linq;
using Plugin.LocalNotifications;
using Xamarin.Forms;

namespace AlarmClock.Views
{
    public partial class AlarmListPage : ContentPage
    {
        DBReader dBReader = DBReader.GetInstance;

        //private bool isDBCalled = false;
        public AlarmListPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            SetAlarms();
        }



        async void SetAlarms()
        {
            var alarms = dBReader.FetchData();
            foreach (var alarmTime in alarms.Result)
            {
                Console.WriteLine("for each");
                if (alarmTime.IsActive == true)
                {
                    Console.WriteLine(alarmTime.Id);
                    Console.WriteLine(alarmTime.IsActive);
                    Console.WriteLine(alarmTime.TimePicked);
                    CrossLocalNotifications.Current.Show("Title", "Details", alarmTime.Id, DateTime.Today + alarmTime.TimePicked);
                    //alarmTime.IsActive = true;
                    Console.WriteLine(alarmTime.Id + " is active " + alarmTime.IsActive);
                    await dBReader.SaveItemAsync<Models.Time>(alarmTime, "Time", alarmTime.Id);
                }
                else
                {
                    Console.WriteLine(alarmTime.Id + " is active " + alarmTime.IsActive);
                }
            }
        }
        //public void ReceiveData(object data, CancellationToken ct)
        //{
        //    //this.alarmList.ItemsSource = Models.Time();
        //}


        protected override async void OnAppearing()
        {
            base.OnAppearing();
            alarmList.ItemsSource = await dBReader.FetchData();
        }

        void Add_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new AddAlarmPage());
        }

        void Alarm_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Models.Time alarm = (Models.Time)alarmList.SelectedItem;
            Navigation.PushAsync(new Views.EditAlarmPage(alarm));
            alarmList.ItemsSource = null;
        }
    }
}
