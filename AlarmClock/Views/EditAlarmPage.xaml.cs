﻿using System;
using System.Collections.Generic;
using AlarmClock.Models;
using AlarmClock.Utilities.Helpers.DatabaseReader;
using Plugin.LocalNotifications;
using Xamarin.Forms;

namespace AlarmClock.Views
{
    public partial class EditAlarmPage : ContentPage
    {
        DBReader dBReader = DBReader.GetInstance;
        Models.Time alarm;

        public EditAlarmPage(Models.Time alarm)
        {
            BindingContext = alarm;
            InitializeComponent();
            this.alarm = alarm;
        }

        async void Save_Clicked(object sender, System.EventArgs e)
        {
            //alarm.Id = alarm.Id;
            //alarm.IsActive = true;
            //alarm.TimePicked = timePicker.Time;
            alarm = new Models.Time() { Id = alarm.Id, IsActive = true, TimePicked = timePicker.Time };
            await dBReader.SaveItemAsync<Models.Time>(alarm, "Time", alarm.Id);
            //CrossLocalNotifications.Current.Show("Alarm", "Wake Up", alarm.Id, DateTime.Today + timePicker.Time);
            //await DisplayAlert("Alarm", "Alarm updated!", "Ok");
            await Navigation.PopAsync();

        }

        async void Delete_Clicked(object sender, System.EventArgs e)
        {
            var alarm = (Time)BindingContext;
            await dBReader.DeleteItemAsync<Time>(alarm);
            await Navigation.PopAsync();

        }
    }
}
