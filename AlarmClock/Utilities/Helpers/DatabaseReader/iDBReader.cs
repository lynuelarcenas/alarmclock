﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AlarmClock.Utilities.Helpers.DatabaseReader
{
    public interface iDBReader
    {
        void CreateTable<ClassName>() where ClassName : new();
        //Task FetchData<ClassName>(CancellationToken ct) where ClassName : new();
        Task<List<Models.Time>> FetchData();
        Task DeleteItemAsync<ClassName>(Object obj) where ClassName : new();
        //Task GetItemAsync<ClassName>() where ClassName : new();
        Task SaveItemAsync<ClassName>(object className, string tableName, int id) where ClassName : new();

    }
}
