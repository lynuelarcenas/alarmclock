﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AlarmClock.Models;
using SQLite;

namespace AlarmClock.Utilities.Helpers.DatabaseReader
{
    public class DBReader : iDBReader
    {
        readonly SQLiteAsyncConnection database;
        public static DBReader dbReader;

        public static DBReader GetInstance
        {
            get
            {
                if (dbReader == null)
                {
                    dbReader = new DBReader(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoSQLite.db3"));
                }

                return dbReader;
            }
        }

        //WeakReference<iDBConnector> _dbReaderDelegate;

        //public iDBConnector DBReaderDelegate
        //{
        //    get
        //    {
        //        iDBConnector dbReaderDelegate;
        //        return _dbReaderDelegate.TryGetTarget(out dbReaderDelegate) ? dbReaderDelegate : null;
        //    }

        //    set
        //    {
        //        _dbReaderDelegate = new WeakReference<iDBConnector>(value);
        //    }

        //}

        public DBReader(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
        }

        public void CreateTable<ClassName>() where ClassName : new()
        {
            database.CreateTableAsync<ClassName>().Wait();
        }

        //public async Task FetchData<ClassName>(CancellationToken ct) where ClassName : new()
        //{
        //    await database.CreateTableAsync<ClassName>();
        //    var list = await database.Table<ClassName>().ToListAsync();
        //    DBReaderDelegate?.ReceiveData(list, ct);
        //}


        public async Task SaveItemAsync<ClassName>(object alarm, string tableName, int Id) where ClassName : new()
        {
            var data = database.QueryAsync<ClassName>("SELECT * FROM [" + tableName + "] WHERE [id] = " + Id);
            if (data.Result.Count == 0)
            {
                await database.InsertAsync(alarm);
            }
            else
            {
                await database.UpdateAsync(alarm);
            }
        }

        public Task<List<Models.Time>> FetchData()
        {
            database.CreateTableAsync<Models.Time>().Wait();
            return database.Table<Models.Time>().ToListAsync();
        }

        public async Task DeleteItemAsync<ClassName>(object obj) where ClassName : new()
        {
            await database.DeleteAsync(obj);
        }
    }

}
