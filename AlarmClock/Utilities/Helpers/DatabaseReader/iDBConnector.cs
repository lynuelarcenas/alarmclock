﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace AlarmClock.Utilities.Helpers.DatabaseReader
{
    public interface iDBConnector
    { 
        void ReceiveData(object something, CancellationToken ct);
    }
}

